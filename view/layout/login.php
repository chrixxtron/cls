<h1> Contact List System </h1>
<hr/>
<h3> Login form </h3>
<form id="login">
	<div class="form-group"> 
		<label for="useremail"> Username / Email Address : </label>
		<input type="text" name="useremail" id='useremail' class="form-control">
	</div>
	<div class="form-group"> 
		<label for="password"> Password : </label>
		<input type="password" name="password" id='password' class="form-control">
		<span>  <a href="index.php?pages=forgot_password"> forget password </a> </span>
	</div>
	<button id="login-btn" class="btn btn-primary"> Login </button>
	<button id="register-btn" class="btn btn-danger"> Sign Up </button>
</form>
<div id="error_msg">
	<div id="error1"> Error : Username and Email Address Should not be blank</div>
	<div id="error2"> Error: Password should not be blank </div>
</div>