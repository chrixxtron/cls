<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login']){ 
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="?pages=admin">ADMIN</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      
      <?php if($users_info['acct_ex']==1) { ?>
      <li class="nav-item">
        <a class="nav-link" href="?pages=users_management">Users</a>
      </li>
      <?php } ?> 
      <li class="nav-item">
        <a class="nav-link" href="?pages=users_account">My Account</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="?pages=__logout">Log Out</a>
      </li>

    </ul>
  </div>
</nav>

<?php
 }
 ?>