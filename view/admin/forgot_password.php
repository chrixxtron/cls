<div class="container">
		
	<h1> Forget Password </h1>
	<div style="margin-top: 50px;">
		
			<div class='form-group'>
				<label for='email'> Email Address </label>
					<input type="email" name="email" id="email" class="form-control">
			</div>
			<div class='form-group'>
				<label for='password'> New Password </label>
					<input type="password" name="password" id="password" class="form-control">
			</div>
			<div class='form-group'>
				<label for='cpassword'> Confirm New Password </label>
					<input type="password" name="cpassword" id="cpassword" class="form-control">
			</div>
			<div class="form-group">
				<button class="btn btn-success" id="change_new_password"> Change New Password  </button>
			</div>
			<span> <a href="?pages=index_page"> I remember my password </a> </span>

	</div>
</div>