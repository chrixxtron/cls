<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==2 && $_SESSION['login'] && $_GET['pages']=='users_purchases_product'){

?>

	<div class="container" style="margin-top: 10px;">
		
		<h1> MY PURCHASES </h1>

		<table class="table table-hover">
				<thead>
					<tr>
						<th> No. </th>
						<th> </th>
						<th> Product Name </th>
						<th> Product Price </th>
						<th> Delivery Details </th>
						<th> Delivery Routes </th>
						<th> Delivery Cost </th>
						<th> Delivery Time </th>
						<th> Total Payment </th>
					</tr>
				</thead>
				<tbody>
					

					<?php 
						$count=1;
						 while($rows=$fetch_deliveries->fetch_assoc()){
						 		extract($rows);
						 	?>
						 	<tr>

						 	<td> <?php echo $count++; ?> </td>
						 	<td> <span style="font-size:20px;"> <i class="fas fa-shopping-cart"></i> </span> </td>
						 	<td> <?php echo $product->products_value($product_id,'name'); ?> </td>
						 	<td> <?php echo number_format($product->products_value($product_id,'cost')); ?> </td>
						 	<td> <?php echo $o_to_d ?> </td>
						 	<td> <?php echo $route_name ?> </td>
						 	<td> <?php echo number_format($cost) ?> </td>
						 	<td> <?php echo $distance." hrs"; ?> </td>
						 	<td> <?php echo number_format($payment); ?> </td>
						 	</tr>

						 <?php
						 }
					?>

				</tbody>
		</table>

	</div>


<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>