<?php
	if($_GET['pages']=='index_register'){ 
?>
		<div style="margin-bottom:20px; margin-top: 20px;">
				<h1> User Registeration </h1>
				<a href="?pages=index_page"> I have Already an Account! </a>
		</div>
		<div style="padding:10px;">

		<form id="new_routes" style="width: 50%;">

			<div class="form-group">
				<label for="fname">  First Name </label>
				<input type="text" name="fname" id="fname" class="form-control">
			</div>

			<div class="form-group">
				<label for="mname">  Middle Name </label>
				<input type="text" name="mname" id="mname" class="form-control">
			</div>

			<div class="form-group">
				<label for="lname">  Last Name </label>
				<input type="text" name="lname" id="lname" class="form-control">
			</div>

			<div class="form-group">
				<label for="uname">  Username  </label>
				<input type="text" name="uname" id="uname" class="form-control">
			</div>

			<div class="form-group">
				<label for="email">  Email Address  </label>
				<input type="email" name="email" id="email" class="form-control">
			</div>

			<div class="form-group">
				<label for="contact">  Contact Number  </label>
				<input type="text" name="contact" id="contact" class="form-control">
			</div>

			<div class="form-group">
				<label for="address">  Complete Address  </label>
				<textarea class='form-control' id='address' name='address'></textarea>
			</div>

			<div class="form-group">
				<label for="country">  Country  </label>
				<select id="country" class="form-control">
					<option value=""> --SELECT COUNTRY </option>
					<?php

						while($rows = $destination->fetch_assoc())
						{
							extract($rows);
							?>

								<option value="<?php echo $id;?>"> <?php echo $country_name ?> </option>

							<?php
						}

					?>
				</select>
			</div>

			<div class="form-group">
				<label for="pass">  Password  </label>
				<input type="password" name="pass" id="pass" class="form-control">
			</div>

			<div class="form-group">
				<label for="cpass">  Confirmed Password  </label>
				<input type="password" name="cpass" id="cpass" class="form-control">
			</div>

			<button id="register_btn" class="btn btn-success"> Sign Up </button>

		</form>

		</div>

<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>