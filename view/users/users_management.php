<?php
	if(isset($_SESSION['user_type']) && $_SESSION['user_type']==1 && $_SESSION['login'] && $_GET['pages']=='users_management' && $users_info['acct_ex']==1){ 
?>

	<div class="container" style="margin-top: 10px;">

		<h1> Users Management </h1>

		<div style="margin-top:20px;">


			<div style="margin-top: 10px; margin-bottom: 10px;">
				<form method="GET" action="#" class="form-inline">
					
					<div class="form-group">
						<input type="hidden" name="pages" value="users_management">
						<input type="text" name="search" class="form-control" placeholder="Search User Email">


					</div>
					<input type="submit" name="_search" value="Search" class="btn btn-success" style="margin-left: 10px;">

				</form>
			</div>


			<table class="table table-hover">

				<thead>
					
					<tr>
						
						<th> No. </th>
						<th> First Name </th>
						<th> Middle Name </th>
						<th> Last Name </th>
						<th> Username </th>
						<th> Email Address	</th>
						<th> Contact Number </th>
						<th> Address </th>
						<th> User Type </th>
						<th> Action </th>

					</tr>

				</thead>

				<tbody>
					<?php 
						$count=1;
						 while($rows=$users_management['data']->fetch_assoc()){
						 		extract($rows);
						 	?>
						 	<a>
						 	<tr>

						 	<td> <?php echo "<a href='?pages=view_users&id=".$id."'>".$count++."</a>"; ?> </td>
						 	<td> <?php echo $fname; ?> </td>
						 	<td> <?php echo $mname; ?> </td>
						 	<td> <?php echo $lname; ?> </td>
						 	<td> <?php echo $username; ?> </td>
						 	<td> <?php echo $email; ?> </td>
						 	<td> <?php echo $contact; ?> </td>
						 	<td> <?php echo ($address) ? $address.", ".$dest->dquery($country_id,'country_name') : $dest->dquery($country_id,'country_name'); ?> </td>
						 	<td> <?php echo ($user_type==1) ? 'Administration' : 'User'; ?> </td>
						 	<td> 
						 		<input type="checkbox" name="user_type" class='user_type' value="<?php echo $id ?>" title='click to change user role' <?php echo $user_type==1 ? 'checked' :''; ?>> <span title='click to change user role'> <i class="fas fa-key"></i> </span> | <a href="?pages=delete_user&id=<?= $id ?>" onclick="return confirm('Are you sure you want to delete this user?')"> <i class="fas fa-trash" title="Delete this user"></i> </a>
						 	</td>

						 	</tr>

						 <?php
						 }
					?>

				</tbody>
				
			</table>
				<?= $users_management['pagination']; ?>
		</div>

	</div>
		
<?php


 } else
 {
 	header('location:?pages=index_page');
 }

?>