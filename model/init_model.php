<?php
	
	// models
	require '../http/db_config.php';
	include_once('users.php');
	include_once('destination.php');
	// header('Access-Control-Allow-Origin');
	// header('Content-Type: application/json');

	class model
	{

		private $get_column;
		private $get_value;
		private $query;
		private $id_value;
		private $count_page;
		public $page_number;
		public $limit_data;

		public function __construct(){
			$this->query = '';
		}

		public function read(){
			
			global $mysqli;

			$table = $this->table;

			$query = "SELECT * FROM `{$table}`";

			$data = $mysqli->query($query);
			return $data;
			
		}

		public function where($value){
			global $mysqli;

			$table = $this->table;

			$where = $value;

			$query = "SELECT * FROM `{$table}` WHERE $where";

			$data = $mysqli->query($query);

			$this->query .= " WHERE ".$where;
			return $data;
		}

		public function input($column,$value){

			$this->get_column.= ($this->get_column) ? ",".$column : $column;
			$this->get_value.= ($this->get_value) ? ","."'".$value."'" : "'".$value."'";
		}

		public function insert()
		{
			global $mysqli;
			$table = $this->table;

			$query = "INSERT INTO $table(".$this->get_column.") VALUES(".$this->get_value.")";

			$data = $mysqli->query($query);
			return $data;
		}

		public function orderBy($column,$order='ASC'){
			
			global $mysqli;

			$table = $this->table;

			$orderby= $this->query.=" ORDER BY `".$column."` ".$order."";

			$query = "SELECT * FROM `{$table}`".$orderby;

			$data = $mysqli->query($query);
			return $data;


		}

		public function paginate($page_no,$limit=10){

			global $mysqli;

			$table =  $this->table;
			if($page_no >= 1 && $limit >= 1){

				$start_from = ($page_no-1) * $limit;

				$count_page = "SELECT count(*) FROM `{$table}`".$this->query;

				$result = $mysqli->query($count_page);

				$row = $result->fetch_array();

				$this->count_page = $row[0];
				$this->page_number = $page_no;
				$this->limit_data = $limit;

				$paginate = $this->query.=" LIMIT $start_from, $limit";


				$query = "SELECT * FROM `{$table}`".$paginate;
				$result = $mysqli->query($query);
				return $result;

			}

			return false;

		}

		public function paginate_page($addition_page=''){

			if($_GET['pages']){

				$total_page = ceil($this->count_page / $this->limit_data);
				$page_link = '';
			  
				for ($i=1; $i<=$total_page; $i++) { 
  						if($i==$this->page_number)  {
   						 $page_link .= "<li class='active'><a href='?pages=".$_GET['pages']."&pagenumber=".$i."'>".$i."</a></li>"; 
  						}
  						else{
    					 $page_link .= "<li><a href='?pages=".$_GET['pages'].
    					 "&pagenumber=".$i."'>".$i."</a></li>"; 
  						}

				}
				

				 return "<ul class='pagination'>".$page_link."</ul>";
		
			}
		}

		public function update($condition=''){
			global $mysqli;

			$column =  explode(',',$this->get_column);
			$value = explode(',', $this->get_value);
			$stmt ='';
			foreach ($column as $index => $columns) {
				
					$stmt.= ($stmt) ? ",".$columns."=".$value[$index] : $columns."=".$value[$index]
					;
				
			}

			if($this->id_value!='' && $this->id!=''){

					$query = "UPDATE ".$this->table." SET ".$stmt." WHERE ".$this->id."=".$this->id_value;
					$query .= ($condition) ? ' AND '.$condition : ''; 
					$data = $mysqli->query($query);
					return $data;

			} else { return "Error Calling: cannot find id's"; }
		}

		public function delete($condition=''){

			global $mysqli;
			$query = 'DELETE FROM '.$this->table.' WHERE '.$this->id.' = '.$this->id_value;
			$query .= ($condition) ? ' AND '.$condition : ''; 
			$data = $mysqli->query($query);
			return $data;

		}
		
		public function id($id){
			$this->id_value = $id;
		}
	}
	// $xquery = new routes;
	// $xquery->id('1');
	// $xquery->input('cost','asad');
	// $xquery->input('name','asads');
	// $num =  $xquery->update();
	// echo $num;
	// echo $xquery->insert();
	// $xquery->read();
	// $fetch =  $xquery->where("username='admin'");
	//$xquery->where("username='admin'");
	//$fetch =  $xquery->orderBy('id','DESC');
	//$num = $fetch->num_rows;
	// echo $num
	//$fetch_array =  $fetch->fetch_assoc();
	//print_r($fetch_array);
	
	// $db = new server_db;
	// $db = $db->connection();
	// $user = new users;
	// $result = $user->read('users');
	// $num = $result->rowCount();

	// if($num > 0){
	// 	// $users = array();
	// 	// $users['data']= array();

	// 	while($row = $result->fetch(PDO::FETCH_ASSOC)){
	// 		extract($row);

	// 		// $user_list = array(
	// 		// 	'id' => $id,
	// 		// 	'username' => $username
	// 		// );

	// 		// array_push($users['data'], $user_list);
	// 	}

	// 	echo $username ;

	// } else {
	// 	echo " NO POST AVAILABLE";
	// }
?>