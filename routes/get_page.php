<?php
	
	// getpages
	class getpages {

		public function source_link($link){

			switch ($link) {
				case 'index_page':
					return 'layout/login.php';
					break;
				case 'index_register':
					return 'users/register.php';
					break;
				case 'admin':
					return 'admin/index.php';
					break;
				case '__logout':
					return '../http/__logout.php';
					break;
				case 'users':
					return 'users/index.php';
					break;
				case 'delete_user':
					return 'admin/delete_users.php';
					break;
				case 'users_account':
					return 'users/my_account.php';
					break;
				case 'users_management':
					return 'users/users_management.php';
					break;
				case 'forgot_password':
					return 'admin/forgot_password.php';
					break;
				case 'view_users':
					return 'admin/view_users.php';
					break;			
				default:
					return '';
					break;
			}
		}

	}

?>