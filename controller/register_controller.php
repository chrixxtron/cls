<?php 
if(isset($_POST['request']) || $_GET['pages']){


	class register_controller
	{

		public function destination()
		{
			$destination = new destination;
			$GLOBALS['destination'] = $destination->read();
		}

		public function show(){

			$users = new users;

			$search = isset($_GET['search']) && isset($_GET['_search']) ? " and email='".$_GET['search']."'"  : '';

			$users->where("acct_ex=0".$search);
			$users->orderBy('id','DESC');
			$data = $users->paginate(isset($_GET['pagenumber']) ? $_GET['pagenumber'] : 1);
			$pagination = $users->paginate_page();
			return array("data"=>$data,"pagination"=>$pagination);

		}

		public function view_users($id){

			$users = new users;

			$data = $users->where("acct_ex=0 AND id={$id}");
			if($data->num_rows){
				return array("data"=>$data);
			}
			return false;
			

		}

		public function users_info($id)
		{
			$users = new users;
			$data = $users->where("id={$id}");
			$result =  $data->fetch_assoc();
			return $result;
		}

		public function users_info_column($id,$column){
			$users = new users;
			$data = $users->where("id={$id}");
			$result =  $data->fetch_assoc();
			return $result[$column];
		}

		public function update($fname,$uname,$email,$address,$country,$password,$oldpassword,$mname,$lname,$contact){

			$users =  new users;
			$destination = new destination;
			$user_id = $_SESSION['login_id'];

			$error='';

			if($oldpassword){
			$oldpassword = md5($oldpassword);
			$password = md5($password);
			$op_exist =  $users->where("password='{$oldpassword}' and id={$user_id}");
				if(!$op_exist->num_rows){
					$error = 'op_wrong';
				}
			}

			$u_exist =  $users->where("username='{$uname}' and id!={$user_id}");
			$e_exist =  $users->where("email='{$email}' and id!={$user_id}");
			
			$d_check = 	$destination->where("id={$country}");

			if($u_exist->num_rows){
				$error = 'u_exist';
			}
			else if($e_exist->num_rows){
				$error = 'e_exist';
			}
			else if(!$d_check->num_rows){
					$error = 'internal';
			}

			if(!$error){
				
				if($oldpassword){

					$users->input('password',$password);
				}
				$users->id($user_id);
				$users->input('fname',$fname);
				$users->input('mname',$mname);
				$users->input('lname',$lname);
				$users->input('username',$uname);
				$users->input('email',$email);
				$users->input('contact',$contact);
				$users->input('country_id',$country);
				$users->input('address',$address);
				if($users->update()){

					 return 'success';
				} else { return 'internal'; }


			}
			else { 
				return $error;
			}

		}

		public function register($fname,$mname,$lname,$uname,$email,$contact,$address,$country,$password)
		{
			$users = new users;
			$destination = new destination;

			$error='';

			$u_exist =  $users->where("username='{$uname}'");
			$e_exist =  $users->where("email='{$email}'");
			$d_check = 	$destination->where("id={$country}");

			if($u_exist->num_rows){
				$error = 'u_exist';
			}
			else if($e_exist->num_rows){
				$error = 'e_exist';
			}
			else if(!$d_check->num_rows){
					$error = 'internal';
			}

			if(!$error){

				$users->input('fname',$fname);
				$users->input('mname',$mname);
				$users->input('lname',$lname);
				$users->input('username',$uname);
				$users->input('password',md5($password));
				$users->input('email',$email);
				$users->input('contact',$contact);
				$users->input('country_id',$country);
				$users->input('address',$address);
				if($users->insert()){

					 return 'success';
				} else { return 'internal'; }


			} else { return $error; }

		}

		public function assign_role($id){

			$users = new users;



			$data = $users->where("id={$id}");

			if($data->num_rows){

				$type = $data->fetch_assoc();
				extract($type);

				$users->id($id);
				$users->input("user_type",($user_type==1) ? '2' : '1');
				$result = $users->update();
				return $result;

			} else {
				return "internal";
			}

		}

		public function forgot_password($email,$password){

			$users = new users;

			$data = $users->where("email='{$email}'");

			if($data->num_rows){
						if($email != 'admin@admin.com'){
							$users->input('password',md5($password));
								if($users->update()){

					 				echo 'success';
										} else { echo 'internal'; }

						} else {
							echo 'internal';
						}
					
			} else {
				echo "ene";
			}
		} 

		public function destroy($id){

			$users = new users;

			$data = $users->where("id={$id}");

			if($data->num_rows){

				$users->id($id);

				if($users->delete()){
					return true;
				} 

			} 

			return false;


		}
		
	}	

	if(isset($_POST['request']['action']) && $_POST['request']['action']=='save')
	{
		require 'controller_init.php';
		
		$register = new register_controller;
		echo $register->register($_POST['request']['fname'],$_POST['request']['mname'],$_POST['request']['lname'],$_POST['request']['uname'],$_POST['request']['email'],$_POST['request']['contact'],$_POST['request']['address'],$_POST['request']['country'],$_POST['request']['pass']);

	}
	if(isset($_POST['request']['action']) && $_POST['request']['action']=='update')
	{
		require 'controller_init.php';
		
		$register = new register_controller;
		echo $register->update($_POST['request']['fname'],$_POST['request']['uname'],$_POST['request']['email'],$_POST['request']['address'],$_POST['request']['country'],$_POST['request']['pass'],$_POST['request']['opass'],$_POST['request']['mname'],$_POST['request']['lname'],$_POST['request']['contact']);

	}
	if(isset($_POST['request']['action']) && $_POST['request']['action']=='forgot_password')
	{
		require 'controller_init.php';

		$register = new register_controller;

		echo $register->forgot_password($_POST['request']['email'],$_POST['request']['password']);

	}
	if(isset($_POST['request']['action']) && $_POST['request']['action']=='assign_role'){

		require 'controller_init.php';
		
		$register = new register_controller;
		echo $register->assign_role($_POST['request']['user_id']);
	}

} else {
	header("../view/?pages=index_page");
}
?>