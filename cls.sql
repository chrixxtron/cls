-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for cls
CREATE DATABASE IF NOT EXISTS `cls` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cls`;

-- Dumping structure for table cls.destination
CREATE TABLE IF NOT EXISTS `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(50) NOT NULL,
  `two_name` varchar(50) NOT NULL,
  `three_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table cls.destination: ~7 rows (approximately)
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` (`id`, `country_name`, `two_name`, `three_name`) VALUES
	(1, 'Philippines', 'PH', 'PHI'),
	(2, 'Japan', 'JP', 'JPN'),
	(3, 'Spain', 'ES', 'ESP'),
	(4, 'Germany ', 'DE', 'DEU'),
	(5, 'Australia', 'AU', 'AUS'),
	(6, 'China ', 'CN', 'CHN'),
	(7, 'Korea', 'KR', 'KOR');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;

-- Dumping structure for table cls.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  `address` tinytext,
  `user_type` enum('1','2') NOT NULL DEFAULT '2',
  `acct_ex` int(1) DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table cls.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `fname`, `mname`, `lname`, `username`, `password`, `email`, `contact`, `country_id`, `address`, `user_type`, `acct_ex`, `active`) VALUES
	(1, 'Admin', '', 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '0912223456', 1, NULL, '1', 1, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
